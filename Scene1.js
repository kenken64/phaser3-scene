class Scene1 extends Phaser.Scene {
    constructor(){
        super("bootGame");
    }

    create(){
        let txt2 = this.add.text(40,20, "Loading game....", {font: "25px Arial", fill: "yellow"});
        txt2.setInteractive().on('pointerdown', function() {
           this.scene.start('playGame');
        }, this);
        
        
    }
}