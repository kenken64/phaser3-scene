const config  = {
    width: 800,
    height: 600,
    backgroundColor: 0x000000,
    scene: [Scene1, Scene2],
    scale: {
        autoCenter: Phaser.Scale.CENTER_BOTH
    }
}

const game  = new Phaser.Game(config);