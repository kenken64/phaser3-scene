# Overview 

Transition from one scene to another using mouseclick event on the first scene.

## Install web server
```
npm i -g http-server
```

## Start the server to serve the app
```
http-server
```